const express = require('express')
const router = express.Router()
const courseController = require('../controllers/courseController.js')
const auth = require('../auth.js')

//route for creating a course

router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    courseController.addCourse(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})

//Retrieve all courses
router.get("/all", (req, res) => {
    courseController.getAllCourse().then(resultFromController => res.send(resultFromController))
})

//Retrieve all active courses
router.get("/", (req, res) => {
    courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})

//Retrieve a course
router.get("/:courseId", (req, res) => {
    courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

//update a course
router.put("/:courseId", auth.verify, (req, res) => {

    courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

//archive a course
router.put("/:courseId/archive", auth.verify, (req, res) => {

    courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})


module.exports = router